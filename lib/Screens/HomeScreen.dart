import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:twilio_flutter/twilio_flutter.dart';

class Homepage extends StatefulWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  TwilioFlutter twilioFlutter = TwilioFlutter(
      accountSid: 'AC353fcf35e86fba4bef932e2cc22c7833',
      authToken: '46756e1f6cbfc87f5c37ca51069a8442',
      twilioNumber: '+16203093834');

  Future<SharedPreferences> _pref = SharedPreferences.getInstance();
  String location = 'Null, Press Button';
  String Address = 'search';
  String? Name;
  String? Email;
  String _policeNumber = '+917977441816';
  String _fireNumber = '+917039266263';
  String _ambulanceNumber = '+919594304859';
  String _emergencyNumber = '+918169581912';
  Future<void> getUserData() async {
    final SharedPreferences sp = await _pref;

    setState(() {
      // _conUserId.text = sp.getString("user_id")!;
      // _conDelUserId.text = sp.getString("user_id")!;
      Name = sp.getString("user_name")!;
      Email = sp.getString("email")!;
      // _conPassword.text = sp.getString("password")!;
    });
  }

  Future<Position> _getGeoLocationPosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      await Geolocator.openLocationSettings();
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  Future<void> GetAddressFromLatLong(Position position) async {
    List<Placemark> placemarks =
        await placemarkFromCoordinates(position.latitude, position.longitude);
    // print(placemarks);
    Placemark place = placemarks[0];
    Address =
        '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    setState(() {});
  }

  @override
  void initState() {
    TwilioFlutter(
        accountSid: 'AC353fcf35e86fba4bef932e2cc22c7833',
        authToken: '46756e1f6cbfc87f5c37ca51069a8442',
        twilioNumber: '+16203093834');
    super.initState();
    getUserData();
  }

  void sendSms(String toNumber, String name, List<String> messageBody) {
    print(toNumber);
    print(messageBody[3]);
    twilioFlutter.sendSMS(
        toNumber: toNumber,
        messageBody: "$name Required At Location : ${messageBody[3]}");
    Navigator.of(context).pop();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: const Text('SMS Sent Successfully'),
        action: SnackBarAction(
          label: '',
          onPressed: () {
            // Some code to undo the change.
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () async {
                  Position position = await _getGeoLocationPosition();
                  location =
                      'Lat: ${position.latitude} , Long: ${position.longitude}';
                  GetAddressFromLatLong(position);
                  // Navigator.of(context).pop();

                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          backgroundColor: Colors.white.withOpacity(0.9),
                          title: const Center(
                            child: Text(
                              'Send SMS',
                            ),
                          ),
                          content: Container(
                            height: MediaQuery.of(context).size.height / 5,
                            padding: const EdgeInsets.all(16),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                color:
                                    const Color(0xFF9FFF67).withOpacity(0.5)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: Text(
                                    'send msg to Police?'.toUpperCase(),
                                    style: const TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ),
                                Row(
                                  children: [
                                    const Text(
                                      'Name ',
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      '$Name',
                                      style: const TextStyle(
                                        fontSize: 18,
                                        // fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                                const Text(
                                  'Address  ',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Flexible(
                                    child: Text(
                                  Address,
                                  style: const TextStyle(fontSize: 16),
                                )),
                                const Text(
                                  'Cordinates  ',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text('$location'),
                              ],
                            ),
                          ),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                    height: 50,
                                    width:
                                        MediaQuery.of(context).size.width / 3.5,
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: Colors.black),
                                    child: const Center(
                                      child: Text(
                                        "NO",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    ))),
                            TextButton(
                                onPressed: () {
                                  sendSms(_policeNumber, 'Police',
                                      [Name!, Email!, location, Address]);
                                },
                                child: Container(
                                    height: 50,
                                    width:
                                        MediaQuery.of(context).size.width / 3.5,
                                    margin: EdgeInsets.only(right: 15),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: Colors.black),
                                    child: const Center(
                                      child: Text(
                                        "YES",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    )))
                          ],
                        );
                      });
                },
                child: helpCard(
                  img: 'assets/images/profile.png',
                  name: 'Police',
                ),
              ),
              GestureDetector(
                onTap: () async {
                  Position position = await _getGeoLocationPosition();
                  location =
                      'Lat: ${position.latitude} , Long: ${position.longitude}';
                  GetAddressFromLatLong(position);
                  // Navigator.of(context).pop();

                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          backgroundColor: Colors.white.withOpacity(0.9),
                          title: Center(
                            child: Text(
                              'Send SMS',
                            ),
                          ),
                          content: Container(
                            height: MediaQuery.of(context).size.height / 5,
                            padding: EdgeInsets.all(16),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                color: Color(0xFF9FFF67).withOpacity(0.5)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: Text(
                                    'send msg to Fire Brigade?'.toUpperCase(),
                                    style: const TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ),
                                Row(
                                  children: [
                                    const Text(
                                      'Name ',
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      '$Name',
                                      style: const TextStyle(
                                        fontSize: 18,
                                        // fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                                const Text(
                                  'Address  ',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Flexible(
                                    child: Text(
                                  '$Address',
                                  style: const TextStyle(fontSize: 16),
                                )),
                                const Text(
                                  'Cordinates  ',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text('$location'),
                              ],
                            ),
                          ),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                    height: 50,
                                    width:
                                        MediaQuery.of(context).size.width / 3.5,
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: Colors.black),
                                    child: const Center(
                                      child: Text(
                                        "NO",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    ))),
                            TextButton(
                                onPressed: () {
                                  sendSms(_fireNumber, 'Fire Brigade',
                                      [Name!, Email!, location, Address]);
                                },
                                child: Container(
                                    height: 50,
                                    width:
                                        MediaQuery.of(context).size.width / 3.5,
                                    margin: EdgeInsets.only(right: 15),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: Colors.black),
                                    child: const Center(
                                      child: Text(
                                        "YES",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    )))
                          ],
                        );
                      });
                },
                child: helpCard(
                  img: 'assets/images/profile.png',
                  name: 'Fire Brigade',
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () async {
                  Position position = await _getGeoLocationPosition();
                  location =
                      'Lat: ${position.latitude} , Long: ${position.longitude}';
                  GetAddressFromLatLong(position);
                  // Navigator.of(context).pop();

                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          backgroundColor: Colors.white.withOpacity(0.9),
                          title: Center(
                            child: Text(
                              'Send SMS',
                            ),
                          ),
                          content: Container(
                            height: MediaQuery.of(context).size.height / 5,
                            padding: EdgeInsets.all(16),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                color: Color(0xFF9FFF67).withOpacity(0.5)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: Text(
                                    'send msg to Ambulance?'.toUpperCase(),
                                    style: const TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ),
                                Row(
                                  children: [
                                    const Text(
                                      'Name ',
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      '$Name',
                                      style: const TextStyle(
                                        fontSize: 18,
                                        // fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                                const Text(
                                  'Address  ',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Flexible(
                                    child: Text(
                                  '$Address',
                                  style: const TextStyle(fontSize: 16),
                                )),
                                const Text(
                                  'Cordinates  ',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text('$location'),
                              ],
                            ),
                          ),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                    height: 50,
                                    width:
                                        MediaQuery.of(context).size.width / 3.5,
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: Colors.black),
                                    child: const Center(
                                      child: Text(
                                        "NO",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    ))),
                            TextButton(
                                onPressed: () {
                                  sendSms(_ambulanceNumber, 'Ambulance',
                                      [Name!, Email!, location, Address]);
                                },
                                child: Container(
                                    height: 50,
                                    width:
                                        MediaQuery.of(context).size.width / 3.5,
                                    margin: EdgeInsets.only(right: 15),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: Colors.black),
                                    child: const Center(
                                      child: Text(
                                        "YES",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    )))
                          ],
                        );
                      });
                },
                child: helpCard(
                  img: 'assets/images/profile.png',
                  name: 'Ambulance',
                ),
              ),
              GestureDetector(
                onTap: () async {
                  Position position = await _getGeoLocationPosition();
                  location =
                      'Lat: ${position.latitude} , Long: ${position.longitude}';
                  GetAddressFromLatLong(position);
                  // Navigator.of(context).pop();

                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          backgroundColor: Colors.white.withOpacity(0.9),
                          title: Center(
                            child: Text(
                              'Send SMS',
                            ),
                          ),
                          content: Container(
                            height: MediaQuery.of(context).size.height / 5,
                            padding: EdgeInsets.all(16),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                color: Color(0xFF9FFF67).withOpacity(0.5)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(bottom: 10),
                                  child: Text(
                                    'send msg to Emergency?'.toUpperCase(),
                                    style: const TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ),
                                Row(
                                  children: [
                                    const Text(
                                      'Name ',
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      '$Name',
                                      style: const TextStyle(
                                        fontSize: 18,
                                        // fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                                const Text(
                                  'Address  ',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Flexible(
                                    child: Text(
                                  '$Address',
                                  style: const TextStyle(fontSize: 16),
                                )),
                                const Text(
                                  'Cordinates  ',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text('$location'),
                              ],
                            ),
                          ),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Container(
                                    height: 50,
                                    width:
                                        MediaQuery.of(context).size.width / 3.5,
                                    padding: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: Colors.black),
                                    child: const Center(
                                      child: Text(
                                        "NO",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    ))),
                            TextButton(
                                onPressed: () {
                                  sendSms(_emergencyNumber, 'Emergency',
                                      [Name!, Email!, location, Address]);
                                },
                                child: Container(
                                    height: 50,
                                    width:
                                        MediaQuery.of(context).size.width / 3.5,
                                    margin: EdgeInsets.only(right: 15),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(16),
                                        color: Colors.black),
                                    child: const Center(
                                      child: Text(
                                        "YES",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                    )))
                          ],
                        );
                      });
                },
                child: helpCard(
                  img: 'assets/images/profile.png',
                  name: 'Emergency',
                ),
              ),
            ],
          ),
        ],
      ),
    )

        // Center(
        //   child: Column(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: [
        //       Text(
        //         'Coordinates Points of $Name \n $Email',
        //         style: const TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
        //       ),
        //       const SizedBox(
        //         height: 10,
        //       ),
        //       Text(
        //         location,
        //         style: const TextStyle(color: Colors.black, fontSize: 16),
        //       ),
        //       const SizedBox(
        //         height: 10,
        //       ),
        //       const Text(
        //         'ADDRESS',
        //         style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
        //       ),
        //       const SizedBox(
        //         height: 10,
        //       ),
        //       Text('${Address}'),
        //       ElevatedButton(
        //           onPressed: () async {
        //             Position position = await _getGeoLocationPosition();
        //             location =
        //                 'Lat: ${position.latitude} , Long: ${position.longitude}';
        //             GetAddressFromLatLong(position);
        //           },
        //           child: const Text('Get Location'))
        //     ],
        //   ),
        // ),
        );
  }
}

class helpCard extends StatelessWidget {
  String? img;
  String? name;
  helpCard({
    this.img,
    this.name,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: SizedBox(
        child: Column(
          children: [
            Image.asset(img!),
            Align(
                alignment: Alignment.bottomCenter,
                child: Center(
                  child: Text(
                    name!,
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ))
          ],
        ),
        height: MediaQuery.of(context).size.height / 4.5,
        width: MediaQuery.of(context).size.width / 2.5,
      ),
    );
  }
}
